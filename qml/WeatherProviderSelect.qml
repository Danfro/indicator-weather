import QtQuick 2.4
import QtQuick.Layouts 1.1
import Lomiri.Components 1.3

ColumnLayout {
    id: root

    property var settings

    spacing: units.gu(1)

    function checkCheckboxes() {
        if (!pirate.checked && !open_weather_map.checked) {
            pirate.checked = true;
        }
    }

    Label {
        text: i18n.tr("Weather Provider")
        Layout.fillWidth: true
    }

    RowLayout {
        Layout.fillWidth: true

        CheckBox {
            id: open_weather_map
            checked: settings.provider == 'open_weather_map'
            onCheckedChanged: {
                if (checked) {
                    settings.provider = 'open_weather_map';
                    pirate.checked = false;
                    open_weather_map.checked = true;
                }

                root.checkCheckboxes();
            }
        }

        Label {
            text: i18n.tr("OpenWeatherMap")

            MouseArea {
                anchors.fill: parent
                onClicked: open_weather_map.checked = true;
            }
        }
    }

    RowLayout {
        Layout.fillWidth: true

        CheckBox {
            id: pirate
            checked: settings.provider == 'pirate'
            onCheckedChanged: {
                if (checked) {
                    settings.provider = 'pirate';
                    pirate.checked = true;
                    open_weather_map.checked = false;
                }

                root.checkCheckboxes();
            }
        }

        Label {
            text: i18n.tr("Pirate Weather")

            MouseArea {
                anchors.fill: parent
                onClicked: pirate.checked = true;
            }
        }
    }
}
